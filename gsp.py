#!/usr/bin/python3
import sys
import gzip


print ('Loading dictionary...')

d = dict()
with gzip.open('dict.gz', 'rt', encoding='utf-8') as f:
    for line in f:
        n, w = line.strip().split('|')
        if not n in d:
            d[n] = []
        d[n].append(w)

print('Ready! Type a number to be encoded, or "q" to quit.')

line = sys.stdin.readline().strip()
while line is not 'q':
    for l in range(3 if len(line) > 3 else len(line), len(line) + 1):
        for i in range(0, len(line) - l + 1):
            p = line[i:i+l]
            if p in d:
                print(str(l) + ':', line[0:i] + '\033[94m' + p + '\033[0m' + line[i+l:len(line)], d[p])
    line = sys.stdin.readline().strip()

