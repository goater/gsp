# GSP

A simple tool to support conversion of numbers into words according to the rules of the [Mnemonic Major System](https://en.wikipedia.org/wiki/Mnemonic_major_system).

## Usage

```python3 gsp.py```

The rest shoudl be self-explanatory.

## Dictionary

Contains a dictionary built for **Polish** language.
Based on https://sjp.pl, licenced under GPL 2, LGPL 2.1, Apache 2.0 and Creative Commons Attribution 4.0 International.

## Licence

MIT.
