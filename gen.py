import re

with open('odm.txt', 'r') as f:
    lines = f.readlines()
    i = 0
    for line in lines:
        words = line.strip().split(', ')
        i += len(words)
        
        for word in words:
            source = word
            word = word.lower()
            word = word.replace("ch", "c").replace("cz", "c").replace("ć", "c")
            word = word.replace("dz", "d").replace("dż", "d").replace("dź", "d")
            word = word.replace("ł", "l")
            word = word.replace("ń", "n")
            word = word.replace("sz", "s").replace("ś", "s")
            word = word.replace("ź", "z").replace("ż", "z")

            word = re.sub(r'[sz]', '0', word)
            word = re.sub(r'[td]', '1', word)
            word = re.sub(r'[n]', '2', word)
            word = re.sub(r'[m]', '3', word)
            word = re.sub(r'[r]', '4', word)
            word = re.sub(r'[l]', '5', word)
            word = re.sub(r'[j]', '6', word)
            word = re.sub(r'[kg]', '7', word)
            word = re.sub(r'[fw]', '8', word)
            word = re.sub(r'[pb]', '9', word)

            word = re.sub(r'[^0-9]', '', word)
            if word is not '':
                print(word + '|' + source)

